// parallax
$(window).bind('scroll',function(e){
    parallaxScroll();
    // parallaxScrollOne()
});
 
function parallaxScroll(){
    var scrolled = $(window).scrollTop();
    $('#parallax_one-first').css('top',(0-(scrolled*.65))+'px');
    $('#parallax_one-second').css('top',(0-(scrolled*.5))+'px');
    $('#parallax_one-third').css('top',(0-(scrolled*.25))+'px');
    $('#parallax_one-last').css('top',(0-(scrolled*.15))+'px');
    $('.parallax_two-bee').css('top',(500+(scrolled*.30))+'px');
    $('.parallax_two-clover_over').css('top',(630+(scrolled*.60))+'px');

}
// function parallaxScrollOne(){
//     var scrolled = $(window).scrollTop();
//     $('#parallax_one-first').css('top',(0-(scrolled*.65))+'px');
//     $('#parallax_one-second').css('top',(0-(scrolled*.5))+'px');
//     $('#parallax_one-third').css('top',(0-(scrolled*.25))+'px');
//     $('#parallax_one-last').css('top',(0-(scrolled*.15))+'px');
// }
// end

// slider
$(window).load(function() {
  $('#slider_one').flexslider({
    animation: "fade",
    prevText: "",       
    nextText: "",
    controlNav: false 
  });
  $(".zoomWindow").css({"background-position":"50% 50%"});
  $("#image_zoom, .zoomContainer").mouseleave(function(){
        

     $(".zoomWindow").css({"background-position":"50% 50%"});
    })
});
// end


 $(document).ready(function(){

    var width_window, width_lop_res = $(window).width();
    var width_lop
    $(window).bind('resize',function(e){
        width_window = $(window).width();

        if(width_window>=550){
            $(".menu_header a").css({"right":"0%"});
            $(".subsection_catalog ul").css({"display":"block"});
        }else{
            $(".menu_header a").css({"right":"-100%"});
        }
        $(".zoomContainer").css({"left": $(".easyzoom_lop").offset().left+170+"px"});
        $(".zoomContainer").css({"top": $(".easyzoom_lop").offset().top+70.46875+"px"});
        // width_lop=width_window-width_lop_res;
        // var zoomContainer_left = parseInt($(".zoomContainer").css("left")) + width_lop;

        // $(".zoomContainer").css({"left": zoomContainer_left+"px"});
    });
    
    $(".menu_header a").hover(function(){
        $(this).find(".menu_header-border").animate({"width": "100%", "left": "0%"}, "fast");
        if(width_window<=550){
            $(this).animate({"right":"-7px"}, "10");
        }
    }, function(){
        $(this).find(".menu_header-border").animate({"width": "0%", "left": "50%"}, "fast");
        if(width_window<=550){$(this).animate({"right":"-12px"}, "10");}
    });

    $(".menu_header-icon").click(function(){
        var menu_ul =  $(this).parent().find("ul");
        if($(this).hasClass("active")) {
            $(this).removeClass("active");
            for(var i=0; i<$(menu_ul).find("li").length; i++){
                var elem_list = $(menu_ul).find("li")[i];
                $(elem_list).find("a").delay(150 * i).animate({
                    "right":"-100%"
                },"1000");
            }
        } else {
            $(this).addClass("active");
            for(var i=0; i<$(menu_ul).find("li").length; i++){
                var elem_list = $(menu_ul).find("li")[i]
                $(elem_list).find("a").delay(150 * i).animate({
                    "right":"-12px"
                },"1000");
            }
            // $(menu_ul).find("li a").each(function(){
            //     $(this).animate({
            //         "right":"-5px"
            //     }, "slow")
            // })
        }
    });


    $(".subsection_catalog-head span").click(function(){
        $(".subsections_catalog ul").slideToggle("slow");
        if($(this).hasClass("active")){
            $(this).removeClass("active")
        }else{
            $(this).addClass("active");
        }
    });

    $(".goods_select-desc").click(function(){
        $(".goods_select-char").removeClass("active");
        $(this).addClass("active");
        $(".card_goods-characteristic").slideUp("300");
        $(".card_goods-description").delay(450).slideDown("300");
    });

    $(".goods_select-char").click(function(){
        $(".goods_select-desc").removeClass("active");
        $(this).addClass("active");
        $(".card_goods-characteristic").delay(450).slideDown("300");
        $(".card_goods-description").slideUp("300");
    });

    // $('.easyzoom').easyZoom();
    $("#image_zoom").elevateZoom({
        zoomWindowWidth:168, zoomWindowHeight:151 
    });
    $("#image_zoom, .zoomContainer").mouseleave(function(){
        

     $(".zoomWindow").css({"background-position":"60% 0"});
    })
});